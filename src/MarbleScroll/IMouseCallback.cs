using MarbleScroll.Interop;

namespace MarbleScroll
{
    public interface IMouseCallback {
        bool Handle(MouseMessages type, MSLLHOOKSTRUCT hookStruct);
    }
}
