﻿using System;
using System.IO;
using System.Configuration;
using System.Reflection;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.Configuration;
using MarbleScroll.Configuration;
using MarbleScroll.Configuration.CommandLine;
using MarbleScroll.Interop;
using static MarbleScroll.Interop.Imports;

namespace MarbleScroll
{
    static class Program
    {
        public static int Main(String[] args)
        {
            Say("Starting");
            var cfg = ParseConfiguration(args);
            if (cfg == null) {
                Say("Unable to parse configuration. Exiting..");
                return 1;
            }
            if (cfg.DumpConfig) {
                Say($"Using config {cfg}");
                return 0;
            }
            if (cfg.ShowHelp) {
                PrintHelp();
                return 1;
            }
            var c = new MouseCallback(cfg.ToMouseCallbackConfiguration());
            using (cfg.Taskbar ? CreateNotificationIcon() : null)
            using (var handler = new MouseCallbackHandler(c))
            {
                Say("Starting main loop");
                Application.Run();
            }
            Say("Stopping");
            return 0;
        }
        private static void Say(String message) => Console.WriteLine(message);
        private static ToolStripItem CreateItem(String text, EventHandler eventHandler) {
            var item = new ToolStripLabel { Text = text };
            item.Click += eventHandler;
            return item;
        }
        private static NotifyIcon CreateNotificationIcon() => new NotifyIcon {
            Visible = true,
            Text  = "MarbleScroll",
            Icon  = Icon.ExtractAssociatedIcon(Assembly.GetExecutingAssembly().Location),
            ContextMenuStrip = new ContextMenuStrip {
                Items = {
                    CreateItem("Exit", (s, e) => {
                        Say("Exit clicked");
                        Application.Exit();
                    })
                }
            },
        };
        private static AppConfiguration? ParseConfiguration(String[] args) =>
            new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appconfig.json", true)
                .AddCommandLine(args, new Dictionary<string, string>() {
                        ["-h"]  = "ShowHelp",
                        ["-v"]  = "Verbose",
                        ["-sx"] = "Sensitivity:X",
                        ["-sy"] = "Sensitivity:Y",
                        ["-dx"] = "Distance:X",
                        ["-dy"] = "Distance:Y",
                        ["-rx"] = "Reset:X",
                        ["-ry"] = "Reset:Y",
                })
                .Build()
                .Get<AppConfiguration>();
        private static void PrintHelp() => Say(String.Join("\n", new[] {
                "Options:",
                " --SensitivityX=int, -sx=int :  Scroll sensitivity in x",
                " -sy  Scroll sensitivity in y",
                " -dx  Scroll distance in x",
                " -dy  Scroll distance in y"}));
    }
}
