using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using MarbleScroll.Interop;
using static MarbleScroll.Interop.Imports;

namespace MarbleScroll
{
    class MouseCallbackHandler : IDisposable {
        private IntPtr _hookID = IntPtr.Zero;
        private LowLevelMouseProc _invokeCallback;
        private bool _disposed = false;
        private MouseCallback _callback;

        public MouseCallbackHandler(MouseCallback callback)
        {
            _callback = callback;
            _invokeCallback = InvokeCallback; // we need this, else gc will collect it
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                var handle = GetModuleHandle(curModule.ModuleName);
                _hookID = SetWindowsHookEx((int)HookId.WH_MOUSE_LL, _invokeCallback, handle, 0);
            }
        }
        ~MouseCallbackHandler() { Dispose(false); }
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing) {
            if (_disposed) return;
            _disposed = true;
            UnhookWindowsHookEx(_hookID);
        }

        private IntPtr InvokeCallback(int nCode, IntPtr wParam, IntPtr lParam) {
            var type = (MouseMessages)wParam;
            var hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT))!;
            if (_callback.Handle(type, hookStruct)) {
                return new IntPtr(1);
            }
            // nothing for me? pass it to next hook
            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }
    }
}
