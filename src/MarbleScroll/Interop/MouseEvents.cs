namespace MarbleScroll.Interop
{
    public enum MouseEvents
    {
        WHEEL       = 0x0800,
        HWHEEL      = 0x1000,
        MOVE        = 0x0001,
        ABSMOVE     = 0x8001,
        MIDDLEDOWN  = 0x0020,
        MIDDLEUP    = 0x0040
    }
}
