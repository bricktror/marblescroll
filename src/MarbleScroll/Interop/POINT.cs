using System;
using System.Runtime.InteropServices;

namespace MarbleScroll.Interop
{
    [StructLayout(LayoutKind.Sequential)]
    public struct POINT
    {
        public int x;
        public int y;

        public static POINT operator -(POINT a, POINT b) => new POINT {
            x = a.x - b.x,
              y = a.y - b.y,
        };
        public static POINT operator +(POINT a, POINT b) => new POINT {
            x = a.x + b.x,
              y = a.y + b.y,
        };
        public static POINT operator *(POINT a, POINT b) => new POINT {
            x = a.x * b.x,
              y = a.y * b.y,
        };
        public static POINT operator /(POINT a, POINT b) => new POINT {
            x = a.x / b.x,
              y = a.y / b.y,
        };
        public static POINT operator %(POINT a, POINT b) => new POINT {
            x = a.x % b.x,
              y = a.y % b.y,
        };
        public static POINT MinXY(POINT a, POINT b)  => new POINT {
            x = Math.Min(a.x, b.x),
              y = Math.Min(a.y, b.y),
        };
        public static POINT MaxXY(POINT a, POINT b)  => new POINT {
            x = Math.Max(a.x, b.x),
              y = Math.Max(a.y, b.y),
        };
        public static POINT IsOverThreshold(POINT point, POINT threshold) =>
            MinXY(MaxXY( point - threshold, new POINT{x=0, y=0}),
                    new POINT{x=1,  y=1});
        public POINT Abs() => new POINT{ x = Math.Abs(x), y = Math.Abs(y), };
        public POINT SignXY() => this / MaxXY(Abs(), new POINT{x=1, y=1});
        public override String ToString() => $"POINT({x}, {y})";
        public bool IsZeroXY() => x== 0 && y==0;
    }
}
