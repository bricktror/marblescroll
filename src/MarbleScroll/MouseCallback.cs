using System;
using MarbleScroll.Interop;
using System.Threading.Tasks;
using MarbleScroll.Configuration;
using static MarbleScroll.Interop.Imports;

namespace MarbleScroll
{
    public class MouseCallback: IMouseCallback {
        public MouseCallback(Configuration cfg) {
            _cfg = cfg;
        }
        private enum ScrollStatus { Off, Pending, Active, }
        private Configuration _cfg;
        private ScrollStatus _scrollStatus = ScrollStatus.Off;
        // when we are simulating middle click
        private bool emulatingMiddleClick = false;

        // some coordinates for detecting scroll
        private POINT _startPt;
        private POINT _dPt;
        private void Debug(params String[] rows) { } // TODO

        public bool Handle(MouseMessages type, MSLLHOOKSTRUCT hookStruct) {
            var pt = hookStruct.pt;
            switch (type) {
                case MouseMessages.WM_MOUSEMOVE:
                case MouseMessages.WM_MOUSEWHEEL:
                case MouseMessages.WM_MOUSEHWHEEL:
                    break;
                default:
                    Debug($"Got type {type} {hookStruct.mouseData}");
                    break;
            }
            uint getXButtonNo() => (hookStruct.mouseData >> 16) & 0b1111_1111_1111_1111;
            switch(type) {
                case MouseMessages.WM_XBUTTONDOWN: {
                    if (getXButtonNo() != 2) return false;
                    StartScrollEmulation(pt);
                    return true;
                }
                case MouseMessages.WM_XBUTTONUP: {
                    if (getXButtonNo() != 2) return false;
                    if (_scrollStatus == ScrollStatus.Active) {
                        _scrollStatus = ScrollStatus.Off;
                    } else {
                        EmulateMiddleClick(pt);
                    }
                    _scrollStatus = ScrollStatus.Off;
                    return true;
                }
                case MouseMessages.WM_MBUTTONDOWN: {
                    if(emulatingMiddleClick) return false;
                    StartScrollEmulation(pt);
                    return true;
                }
                case MouseMessages.WM_MBUTTONUP: {
                    if (emulatingMiddleClick) {
                        emulatingMiddleClick = false;
                        return false;
                    }
                    bool isActive = _scrollStatus == ScrollStatus.Active;
                        _scrollStatus = ScrollStatus.Off;
                    return isActive;
                }
                case MouseMessages.WM_MOUSEMOVE: {
                    if (_scrollStatus == ScrollStatus.Off) return false;
                    EmulateScrollMoveBy(_startPt - pt);
                    return true;
                }
            }
            return false;
        }
        private void EmulateMiddleClick(POINT pt) {
            Debug("Emulating middle mouse click");
            emulatingMiddleClick = true;
            SendMouseEvent(
                    new Event { Type = MouseEvents.MIDDLEDOWN, Point = pt },
                    new Event { Type = MouseEvents.MIDDLEUP, Point = pt });
        }
        private void StartScrollEmulation(POINT pt) {
            Debug("Pending scroll emulation");
            _scrollStatus = ScrollStatus.Pending;
            _startPt = pt;
            _dPt = new POINT { x=0, y=0 };

            // for scrolling in window under mouse pointer
            var focusWindow = WindowFromPoint(_startPt);
            var foregroundWindow = GetForegroundWindow();

            // only focus is window is not already focused
            if (GetAncestor(foregroundWindow, 3) != GetAncestor(focusWindow, 3)) {
                Debug("Shifting window focus");
                SetForegroundWindow(focusWindow);
            }
        }
        private void EmulateScrollMoveBy(POINT pt) {
            var dpt =_dPt + pt;
            _dPt = dpt % _cfg.Sensitivity;
            var distance = (dpt / _cfg.Sensitivity) * _cfg.Distance;
            if (distance.IsZeroXY()) return;
            if (_scrollStatus != ScrollStatus.Active) {
                Debug("Scroll is now active");
                _scrollStatus = ScrollStatus.Active;
            }
            if (distance.x != 0) {
                SendMouseEvent(new Event{ Type = MouseEvents.HWHEEL, Data = distance.x});
                if (_cfg.ResetYOnX) _dPt.y = 0;
            }
            if (distance.y != 0) {
                SendMouseEvent(new Event{ Type = MouseEvents.WHEEL, Data = distance.y});
                if (_cfg.ResetXOnY) _dPt.x = 0;
            }
        }
        private void SendMouseEvent(params Event[] events) {
            // need to run in different thread, as it takes to long to execute mouse_event and windows doesn't like it
            Task.Factory.StartNew(() => {
                foreach (var ev in events) {
                    mouse_event((uint)ev.Type, (uint)ev.Point.x, (uint)ev.Point.y, ev.Data, UIntPtr.Zero);
                }
            });
        }
        private struct Event {
            public MouseEvents Type;
            public POINT Point;
            public int Data;
        }
        public struct Configuration {
            public POINT Sensitivity;
            public POINT Distance;
            public bool Verbose;
            public bool ResetXOnY;
            public bool ResetYOnX;
        }
    }
    public static class MouseCallbackConfigurationExtensions {
        public static POINT ToInteropPoint(this Point pt) => new POINT { x = pt.X, y = pt.Y };
        public static MouseCallback.Configuration ToMouseCallbackConfiguration(this AppConfiguration config) =>
            new MouseCallback.Configuration {
                Sensitivity = config.Sensitivity.ToInteropPoint(),
                Distance = config.Distance.ToInteropPoint(),
                ResetXOnY = config.Reset.X == AxisResetMode.ResetOnOtherAxis,
                ResetYOnX = config.Reset.Y == AxisResetMode.ResetOnOtherAxis,
            };
    }
}
