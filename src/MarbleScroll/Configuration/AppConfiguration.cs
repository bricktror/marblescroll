using System;

namespace MarbleScroll.Configuration
{
    public class AxisResetConfiguration
    {
        public AxisResetMode X { get; set; }
        public AxisResetMode Y { get; set; }
        public override string ToString()  => $"AxisResetConfiguration({X}, {Y})";
    }

    public class AppConfiguration
    {
        public Point Sensitivity { get; set; } = new Point { X = 30, Y = 30 };
        public Point Distance { get; set; } = new Point { X = 100, Y = 100 };
        public AxisResetConfiguration Reset { get; set; } = new AxisResetConfiguration();
        public bool Taskbar { get; set; } = true;
        public bool Verbose { get; set; } = false;
        public bool ShowHelp { get; set; } = false;
        public bool DumpConfig { get; set; } = false;
        public override String ToString() => "AppConfiguration("+ String.Join(", ", new [] {
                $"Verbose={Verbose}",
                $"ShowHelp={ShowHelp}",
                $"DumpConfig={DumpConfig}",
                $"Sensitivity={Sensitivity}",
                $"Distance={Distance}",
                $"Reset={Reset}",
        }) + ")";
    }
}
