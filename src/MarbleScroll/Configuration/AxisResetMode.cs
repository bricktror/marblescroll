namespace MarbleScroll.Configuration
{
    public enum AxisResetMode
    {
        NoReset,
        ResetOnOtherAxis
    }
}
