using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace MarbleScroll.Configuration.CommandLine
{
    public class CommandLineConfigurationSource : IConfigurationSource {
        public IEnumerable<string> Args { get; }
        public IDictionary<string, string> SwitchMap { get; }
        public CommandLineConfigurationSource(IEnumerable<string> args, IDictionary<string, string> switchMap) {
            Args = args;
            SwitchMap = switchMap;
        }
        public IConfigurationProvider Build(IConfigurationBuilder builder) =>
            new CommandLineConfigurationProvider(Args, SwitchMap);
    }
}
