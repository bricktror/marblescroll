using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace MarbleScroll.Configuration.CommandLine
{
    public static class CommandLineConfigurationExtensions
    {
        public static IConfigurationBuilder AddCommandLine(
                this IConfigurationBuilder configurationBuilder,
                string[] args,
                IDictionary<string, string> switchMappings
        ) {
            configurationBuilder.Add(new CommandLineConfigurationSource(args, switchMappings));
            return configurationBuilder;
        }
    }
}
