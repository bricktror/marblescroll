using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace MarbleScroll.Configuration.CommandLine
{
    public class CommandLineConfigurationProvider : ConfigurationProvider
    {
        private readonly IDictionary<string, string> _switchMap;
        private readonly IEnumerable<string>  _args;
        public CommandLineConfigurationProvider(IEnumerable<string> args, IDictionary<string, string> switchMap) {
            _args      = args;
            _switchMap = switchMap;
        }
        public override void Load() {
            var data  = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            String? lastKey = null;
            foreach (var arg in _args)
            {
                var (key, value) = ParseArg(arg);
                if (key == null && value == key) continue; // Should not happen
                if (key == null)
                {
                    key = lastKey;
                    lastKey = null;
                }
                else
                {
                    MaterializeLastAsFlag();
                    key = SanitizeKey(key);
                }
                if (key == null) continue;
                if (value != null)
                {
                    data[key] = value;
                }
                else
                {
                    lastKey = key;
                }
            }
            MaterializeLastAsFlag();
            Data = data;

            (string? key, string? value) ParseArg(string arg) {
                var parts = arg.Split("=", count: 2);
                return parts.Length switch
                {
                    1 => parts[0].StartsWith("-")
                        ? (parts[0], (string?)null)
                        : (null, parts[0]),
                    2 => (parts[0], parts[1]),
                    _ => (null, null),
                };
            }
            string? SanitizeKey(string key)
                => _switchMap.TryGetValue(key, out var newKey) ? newKey
                : key.StartsWith("--") ? key.Substring(2).Replace(".", ConfigurationPath.KeyDelimiter)
                : null;
            void MaterializeLastAsFlag()
            {
                if (lastKey == null) return;
                data[lastKey] = "true";
            }
        }
    }
}
